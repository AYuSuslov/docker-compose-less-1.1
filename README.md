# Описание задания

https://docs.docker.com/engine/install/centos/
https://docs.docker.com/compose/install/
Приложения джанго
https://gitlab.com/chumkaska1/django_blog.git
Задача: написать докер композ, в котором будет 3 сервиса:
- nginx
- postgresql
- app (само приложение) (докер файл с инсталяцией зависимостей)

nginx port 8000:8000

Создать нетворк для них

Ресурсы
https://realpython.com/django-development-with-docker-compose-and-machine/ ( смотреть только как пример docker-compose !)
https://docs.docker.com/compose/networking/


В ответе ссылку к вашему приложению.
Ссылку на гит репу в гилабе где будет приложение + докерфайл и докер композ


Присоздании экземпляра ВМ в облаке по дефолту открыт только 22 порт в зависимости о выбранного облако необходимо в разделе правил фаервола открыть 8000 порт для всех Айпи адресов 0.0.0.0\0.


# Ответ

Ссылка на работающее приложение: http://35.246.243.46:8000/
Ссылка на репозиторий *Gitlab* (приложение + Dockerfile приложения + docker-compose.yml файл): https://gitlab.com/AYuSuslov/docker-compose-less-1.1

Под проект создана отдельная внешняя сеть (network) *django_blog_network*. Compose подключает все контейнеры к этой сети.
Имена приложений заданы как в задании: *nginx*, *postgresql* и *app*. Для правильности ссылок используются линки.
Приложение запускается стандартным образом через docker-compose ($ docker-compose up -d --build).

